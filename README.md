### BlogExample ###

Sample application created on "Ruby on Rails" to apply knowledge acquired on "Web Application Architectures" course. 

#Setting Up Your Development Environment#
The instructions for installing RVM, Ruby and Rails are the following sites:

* [http://railsapps.github.io/installing-rails.html](http://railsapps.github.io/installing-rails.html)

* [https://gist.github.com/pcjpcj2/5443017](https://gist.github.com/pcjpcj2/5443017)